/*
 * rfc1662Encoder.cpp
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#include "main.h"
#include "rfc1662Encoder.hpp"
#include "protocol-utils-constants.h"
#include "protocol-utils-escaping.h"
//#include "crc8.h"
#include "jeti_decoder_crc.hpp"

Rfc1662::Rfc1662Encoder::Rfc1662Encoder(void (*p_Callback)(uint8_t *&, uint8_t*)):
		p_Callback(p_Callback) {}

void Rfc1662::Rfc1662Encoder::encodeFraming(uint8_t *& p_Head, uint8_t* p_Tail, uint8_t protocolKey, uint8_t payloadLength) {
	if (p_Callback == NULL || p_Tail < p_Head) {
		return;
	}
	uint8_t maxLength = 30;
	uint8_t buffer[maxLength];
	uint8_t * p_Dest = buffer;
	uint8_t * p_DestTail = buffer + maxLength;
	uint8_t crc = 0;
	*(p_Dest++) = RFC_1662_FRAME_FLAG;
	*(p_Dest++) = protocolKey;
	*(p_Dest++) = payloadLength;
	crc = calculateCrc(payloadLength, crc);
	do {
		if (protocol_utils::shouldEscape(*p_Head)) {
			protocol_utils::writeEscaped(p_Dest, *p_Head);
			crc = calculateCrc(*(p_Dest - 1), crc);
		} else {
			*(p_Dest) = *p_Head;
		}
		crc = calculateCrc(*p_Dest, crc);
	} while (p_Head++ < p_Tail && ++p_Dest< p_DestTail - 1);
	*(++p_Dest) = crc;
	p_DestTail = p_Dest;
	p_Dest = buffer;
	p_Callback(p_Dest, p_DestTail);
}

