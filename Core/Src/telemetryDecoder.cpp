/*
 * telemetryDecoder.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryDecoder.h"
#include "telemetryDecoderDelegate.h"
#include "abstractPayloadDecoder.hpp"
#include "protocol_config.h"

using namespace Telemetry;

Decoder::Decoder(DecoderDelegate * p_Delegate):
		AbstractPayloadDecoder(8) {
	this->p_Delegate = p_Delegate;
}

uint8_t Decoder::getProtocolId() {
	return TELEMETRY_PROTOCOL_ID;
}

uint8_t Decoder::getContentLength(uint8_t typeKey) {
	switch (typeKey) {
	case TELEMETRY_PROTOCOL_OBSERVING_ACTIVATE:
	case TELEMETRY_PROTOCOL_OBSERVING_DEACTIVATE:
	case TELEMETRY_PROTOCOL_SCAN_START:
	case TELEMETRY_PROTOCOL_SCAN_END:
	case TELEMETRY_PROTOCOL_SCAN_GET_IDENTIFIED:
		return 1;
	default:
		return 0;
	}
}

void Decoder::payloadComplete(uint8_t payloadLength) {
	switch (*p_ContentBuffer) {
	case TELEMETRY_PROTOCOL_OBSERVING_ACTIVATE:
		p_Delegate->activateTelemetryTransfer();
		break;
	case TELEMETRY_PROTOCOL_OBSERVING_DEACTIVATE:
		p_Delegate->deactivateTelemetryTransfer();
		break;
	case TELEMETRY_PROTOCOL_SCAN_START:
		p_Delegate->scanSensors();
		break;
	case TELEMETRY_PROTOCOL_SCAN_GET_IDENTIFIED:
		p_Delegate->requestScannedSensors();
		break;
	case TELEMETRY_PROTOCOL_SCAN_END:
		p_Delegate->stopSensorScan();
		break;
	case TELEMETRY_PROTOCOL_SUBSCRIBE:
		p_Delegate->subscribeSensor(
				decodeUint16(p_ContentBuffer + 1),
				decodeUint16(p_ContentBuffer + 3),
				*(p_ContentBuffer + 5),
				*(p_ContentBuffer + 6));
		break;
	case TELEMETRY_PROTOCOL_UNSUBSCRIBE:
		p_Delegate->unsubscribeSensor(
				decodeUint16(p_ContentBuffer + 1),
				decodeUint16(p_ContentBuffer + 3),
				*(p_ContentBuffer + 5));
		break;
	case TELEMETRY_PROTOCOL_SUBSCRIBED_VALUES:
		p_Delegate->requestSubscriptionValues();
		break;
	default:
		return;
	}
}

uint16_t Decoder::decodeUint16(uint8_t * p_Value) {
	return (*p_Value << 8) + *(p_Value + 1);
}



