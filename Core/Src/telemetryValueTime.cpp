/*
 * telemetryValueTime.cpp
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryValueTime.h"
#include "protocol_config.h"

using namespace Telemetry;

TimeValue::TimeValue(uint8_t hour, uint8_t minute, uint8_t second):
		hour(hour), minute(minute), second(second)
{

}

uint8_t TimeValue::getTelemetryType() {
	return TELEMETRY_PROTOCOL_TYPE_TIME;
}

void TimeValue::update(jeti_decoder::ExData valueUpdate) {
	jeti_decoder::ExDataTime timeUpdate = valueUpdate.time;
	hour = timeUpdate.hour;
	minute = timeUpdate.minute;
	second = timeUpdate.second;
}


