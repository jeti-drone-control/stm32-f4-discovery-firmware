/*
 * config.c
 *
 *  Created on: 3 Aug 2020
 *      Author: florian.siegel
 */

#include "application-tasks.h"
#include "config.h"
#include "cmsis_os2.h"
#include "jeti_decoder_telemetry_ex_text.hpp"
#include "controlStore.h"

/* Definitions for defaultTask */
extern osThreadId_t defaultTaskHandle;
extern osThreadId_t bleRxTaskHandle;
extern osThreadId_t protocolDecoderHandle;
extern osThreadId_t connectivityTasHandle;
extern osThreadId_t bleTxTaskHandle;
extern osThreadId_t jetiRxHandle;
extern osThreadId_t controlTask;

extern osMessageQueueId_t bleRxQueueHandle;
extern osMessageQueueId_t bleConnectivityRxQueueHandle;
extern osMessageQueueId_t bleTxQueueHandle;
extern osMessageQueueId_t jetiRxQueueHandle;
extern osMessageQueueId_t identifiedSensorsQueue;

extern osTimerId_t pulseTimer;

extern "C" {

const osThreadAttr_t defaultTask_attributes = {
  .name = "default",
  .stack_size = 128,
  .priority = (osPriority_t) osPriorityNormal
};

/* Definitions for bleRxTask */
const osThreadAttr_t bleRxTask_attributes = {
  .name = "bleRx",
  .stack_size = 1024,
  .priority = (osPriority_t) osPriorityNormal
};

const osThreadAttr_t bleTxTask_attributes = {
  .name = "bleTx",
  .stack_size = 1024,
  .priority = (osPriority_t) osPriorityHigh
};

/* Definitions for protocolDecoder */
const osThreadAttr_t protocolDecoder_attributes = {
  .name = "protocolDecoder",
  .stack_size = 128,
  .priority = (osPriority_t) osPriorityNormal
};
/* Definitions for connectivityTas */
const osThreadAttr_t connectivityTas_attributes = {
  .name = "connectivity",
  .stack_size = 128,
  .priority = (osPriority_t) osPriorityHigh
};

const osThreadAttr_t jetiRxTask_attributes = {
  .name = "jetiRx",
  .stack_size = 2048,
  .priority = (osPriority_t) osPriorityHigh
};

const osThreadAttr_t controlTask_attributes = {
  .name = "controlTask",
  .stack_size = 1024,
  .priority = (osPriority_t) osPriorityRealtime
};



/* Definitions for bleRxQueue */
const osMessageQueueAttr_t bleRxQueue_attributes = {
  .name = "bleRx"
};

const osMessageQueueAttr_t bleTxQueue_attributes = {
  .name = "bleTx"
};

/* Definitions for bleConnectivityRxQueue */
const osMessageQueueAttr_t bleConnectivityRxQueue_attributes = {
  .name = "bleConnectivityRx"
};

const osMessageQueueAttr_t jetiRxQueue_attributes = {
  .name = "jetiRx"
};

const osMessageQueueAttr_t identifiedSensors_attributes = {
  .name = "identifiedSensors"
};

const osTimerAttr_t pulseTimerAttributes = {
		.name = "pulseTimer"
};

}


void configureFreeRtosTasks() {
	//defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);
	bleRxTaskHandle = osThreadNew(startBleRxTask, NULL, &bleRxTask_attributes);
	bleTxTaskHandle = osThreadNew(startBleTxTask, NULL, &bleTxTask_attributes);
	//protocolDecoderHandle = osThreadNew(startProtocolDecoderTask, NULL, &protocolDecoder_attributes);
	//connectivityTasHandle = osThreadNew(startConnectivityTask, NULL, &connectivityTas_attributes);
	jetiRxHandle = osThreadNew(startJetiRxTask, NULL, &jetiRxTask_attributes);
	controlTask = osThreadNew(startControlTask, NULL, &controlTask_attributes);
}

void configureFreeRtosQueues() {
	  bleRxQueueHandle = osMessageQueueNew (32, sizeof(uint8_t), &bleRxQueue_attributes);
	  bleConnectivityRxQueueHandle = osMessageQueueNew (16, sizeof(uint16_t), &bleConnectivityRxQueue_attributes);
	  bleTxQueueHandle = osMessageQueueNew (64, sizeof(uint8_t), &bleTxQueue_attributes);
	  jetiRxQueueHandle = osMessageQueueNew (128, sizeof(uint16_t), &jetiRxQueue_attributes);

	  // This is optional - could be destroyed
	  identifiedSensorsQueue = osMessageQueueNew(10, sizeof(jeti_decoder::ExTextTelemetry*), &identifiedSensors_attributes);
	  /*
	  vQueueAddToRegistry(bleConnectivityRxQueueHandle);
	  vQueueAddToRegistry(bleRxQueueHandle);
	  vQueueAddToRegistry(bleTxQueueHandle);
	  */
}

