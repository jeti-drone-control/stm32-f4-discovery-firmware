/*
 * controlStore.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "controlStore.h"
#include "controlDecoder.h"
#include "cmsis_os2.h"

using namespace Control;

extern Control::Store controlStore;

Store::Store(StoreDelegate * p_Delegate,
		void (*applyPulseCallback)(bool isPulseHigh),
		void (*startTimerCallback)(),
		void (*stopTimerCallback)()):
	p_Delegate(p_Delegate),
	isControlling(false),
	isPulseHigh(false),
	applyPulseCallback(applyPulseCallback),
	startTimerCallback(startTimerCallback),
	stopTimerCallback(stopTimerCallback),
	maxChannelId(MINIMUM_CHANNELS - 1),
	totalFrameLength(0)
{
}

void Store::activateControlSignal() {
	isControlling = true;
	if (p_Delegate != NULL) {
		channelCount = 0;
		p_Delegate->ack();
		startTimerCallback();
	}
}

void Store::deactivateControlSignal() {
	isControlling = false;
	if (p_Delegate != NULL) {
		p_Delegate->ack();
		stopTimerCallback();
	}
}

void Store::setChannelValue(uint8_t channelId, float value) {
	if (channelId > maxChannelId && channelId < MAXIMUM_CHANNELS - 1) {
		maxChannelId = channelId;
	}
	channelValues[channelId] = value;
}

uint64_t Store::pulseDurationExceeded() {
	if (!isControlling) {
		applyPulseCallback(true);
		return DEFAULT_CONTROL_PULSE_PERIOD_US;
	}

	if (!isPulseHigh) {
		applyPulseCallback(true);
		isPulseHigh = true;
		return PPM_VALUE_SEPARATION_LENGTH_US;
	}

	applyPulseCallback(false);
	isPulseHigh = false;

	uint8_t size = channelValues.size();
	std::map<int, float>::iterator it = channelValues.begin();
	if (channelCount <= maxChannelId) {
		float channelValue = channelValues[channelCount++];

		uint16_t pulseLengthUs = CALCULATE_PPM_PULSE_LENGTH_US(channelValue / 100);
		totalFrameLength += pulseLengthUs;
		return pulseLengthUs;
	} else {
		channelCount = 0;
		totalFrameLength = 0;
		return MINIMUM_PPM_SYNCHRONZIATION_LENGTH_US;
	}
}
