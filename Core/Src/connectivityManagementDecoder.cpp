/*
 * pingPongDecoder.cpp
 *
 *  Created on: 4 Aug 2020
 *      Author: florian.siegel
 */

#include "protocol_config.h"
#include "connectivityManagementDecoder.h"

ConnectivityManagement::Decoder::Decoder(ConnectivityManagement::DecoderDelegate * p_Delegate):
	AbstractPayloadDecoder(8) {
	this->p_Delegate = p_Delegate;
};


uint8_t ConnectivityManagement::Decoder::getContentLength(uint8_t typeKey) {
	switch (typeKey) {
	case CONNECTIVITY_PING_MESSAGE_KEY:
		return 1;
	case CONNECTIVITY_PROTOCOL_RESET:
		return 1;
	default:
		return 0;
	}
}

uint8_t ConnectivityManagement::Decoder::getProtocolId() {
	return CONNECTIVITY_PROTOCOL_ID;
}

void ConnectivityManagement::Decoder::payloadComplete(uint8_t payloadLength) {
	if (this->p_Delegate == NULL) {
		return;
	}

	switch (*p_ContentBuffer) {
	case CONNECTIVITY_PROTOCOL_STATUS_REQUEST:
		p_Delegate->connectivityStatusRequest();
		break;
	case CONNECTIVITY_PROTOCOL_RESET:
		p_Delegate->resetRequired();
	default:
		return;
	}
}
