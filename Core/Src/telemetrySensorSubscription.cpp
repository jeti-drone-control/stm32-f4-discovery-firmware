/*
 * telemetrySensorSubscription.cpp
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#include <telemetryValueNumericValue.h>
#include "telemetrySensorSubscription.h"
#include "jeti_decoder_telemetry_type.hpp"
#include "telemetryValueDate.h"
#include "telemetryValueGps.h"
#include "telemetryValueTime.h"
#include "jeti_decoder_data.hpp"


using namespace Telemetry;

SensorSubscription::SensorSubscription(uint8_t subscriptionId):
		subscriptionId(subscriptionId),
		p_Value(NULL) {}

void SensorSubscription::update(jeti_decoder::ExDataTelemetry * p_Telemetry) {
	if (p_Value != NULL) {
		p_Value->update(p_Telemetry->getData());
	}

	jeti_decoder::ExData value = p_Telemetry->getData();

	switch (p_Telemetry->getDataType()) {
	case jeti_decoder::ExDataType::NUMERIC_VALUE:
		p_Value = new NumericValue(value.numeric);
		break;
	case jeti_decoder::ExDataType::GPS_VALUE:
		p_Value = new GpsValue(
				value.gps.position,
				value.gps.orientation);
		break;
	case jeti_decoder::ExDataType::TIME_VALUE:
		p_Value = new TimeValue(
				value.time.hour,
				value.time.minute,
				value.time.second);
		break;
	case jeti_decoder::ExDataType::DATE_VALUE:
		p_Value = new DateValue(
				value.date.day,
				value.date.month,
				value.date.year);
		break;
	}
}

TelemetryValueInterace* SensorSubscription::getValue()
{
	return p_Value;
}
