/*
 * crc8.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#define CRC_POLYNOMINAL 0x07

#include "main.h"

/*
uint8_t calculateCrc(uint8_t value, uint8_t crcSeed)
{
	uint8_t crc_u = value;
	uint8_t i;
	crc_u ^= crcSeed;
	for (i=0; i<8; i++)
	{
		crc_u = ( crc_u & 0x80 )
				? CRC_POLYNOMINAL ^ ( crc_u << 1 )
						: ( crc_u << 1 );
	}
	return crc_u;
}
*/

