/*
 * telemetryValueDate.cpp
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryValueDate.h"
#include "protocol_config.h"

using namespace Telemetry;

DateValue::DateValue(uint8_t day, uint8_t month, uint16_t year):
		day(day), month(month), year(year)
{};

uint8_t DateValue::getTelemetryType() {
	return TELEMETRY_PROTOCOL_TYPE_DATE;
}

void DateValue::update(jeti_decoder::ExData valueUpdate) {
	jeti_decoder::ExDataDate dateUpdate = valueUpdate.date;
	day = dateUpdate.day;
	month = dateUpdate.month;
	year = dateUpdate.year;
}

