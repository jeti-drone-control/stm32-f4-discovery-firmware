/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

#ifndef TEST

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "portmacro.h"
#include "task.h"
#include "config.h"
#include "rfc1662Decoder.hpp"
#include "rfc1662Encoder.hpp"
#include "connectivityManagementDecoder.h"
#include "connectivityManagementStore.h"
#include "connectivityManagementEncoder.h"
#include "telemetryDecoder.h"
#include "telemetryStore.h"
#include "telemetryEncoder.h"
#include "controlDecoder.h"
#include "controlStore.h"
#include "controlEncoder.h"
#include "jeti_decoder_decoder_class.hpp"
#include "callbacks.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* Free RTOS entities  */
osThreadId_t defaultTaskHandle;
osThreadId_t bleRxTaskHandle;
osThreadId_t bleTxTaskHandle;
osThreadId_t protocolDecoderHandle;
osThreadId_t connectivityTasHandle;
osThreadId_t jetiRxHandle;
osThreadId_t controlTask;

osMessageQueueId_t bleRxQueueHandle;
osMessageQueueId_t bleTxQueueHandle;
osMessageQueueId_t bleConnectivityRxQueueHandle;
osMessageQueueId_t jetiRxQueueHandle;
osMessageQueueId_t identifiedSensorsQueue;

TIM_HandleTypeDef pulseTimer;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void initializeTimer();
void transmitMessage(uint8_t *& p_Head, uint8_t * p_Tail);
void applyPulse(bool isPulseHigh);
void startPulseTimer();
void stopPulseTimer();
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);

void * operator new( size_t size )
{
    return pvPortMalloc( size );
}

void * operator new[]( size_t size )
{
    return pvPortMalloc(size);
}

void operator delete( void * ptr )
{
    vPortFree ( ptr );
}

void operator delete[]( void * ptr )
{
    vPortFree ( ptr );
}

/* USER CODE BEGIN PFP */
extern "C" BaseType_t xTaskResumeFromISR( TaskHandle_t xTaskToResume );
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
Rfc1662::Rfc1662Encoder frameEncoder = Rfc1662::Rfc1662Encoder(&transmitMessage);

Telemetry::Encoder telemetryEncoder = Telemetry::Encoder(&frameEncoder);
Telemetry::SystemDelegate telemetryStoreSystemDelegate = Telemetry::SystemDelegate();
Telemetry::Store telemetryStore = Telemetry::Store(
		&telemetryEncoder, &telemetryStoreSystemDelegate);
Telemetry::Decoder telemetryDecoder = Telemetry::Decoder(&telemetryStore);

ConnectivityManagement::Encoder connectivityManagementEncoder = ConnectivityManagement::Encoder(&frameEncoder);
ConnectivityManagement::ConnectivityManagementStore connectivityStore =
		ConnectivityManagement::ConnectivityManagementStore(
				&connectivityManagementEncoder,
				&telemetryStore);
ConnectivityManagement::Decoder connectivityManagementDecoder = ConnectivityManagement::Decoder(&connectivityStore);

uint16_t pulseTickCount = 0;
Control::Encoder controlEncoder = Control::Encoder(&frameEncoder);
Control::Store controlStore = Control::Store(
		&controlEncoder,
		applyPulse,
		startPulseTimer,
		stopPulseTimer);
Control::Decoder controlDecoder = Control::Decoder(&controlStore);

std::map<uint8_t, Framing::PayloadDecoderInterface*> payloadDecoders;
rfc1662::Rfc1662Decoder frameDecoder = rfc1662::Rfc1662Decoder(&payloadDecoders);

jeti_decoder::Decoder jetiDecoder = jeti_decoder::Decoder();

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	payloadDecoders[connectivityManagementDecoder.getProtocolId()] = &connectivityManagementDecoder;
	payloadDecoders[telemetryDecoder.getProtocolId()] = &telemetryDecoder;
	payloadDecoders[controlDecoder.getProtocolId()] = &controlDecoder;

	jetiDecoder.registerDecodingCompleteHandler(&telemetryStore);
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */
  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  configureFreeRtosQueues();
  telemetryStoreSystemDelegate.identifiedSensorsQueue = identifiedSensorsQueue;

  /* Create the thread(s) */
  configureFreeRtosTasks();
  //configureFreeRtosTimer();
  initializeTimer();

  /* Start scheduler */
  osKernelStart();
 
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */
  __USART2_CLK_ENABLE();
  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_9B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);

	/*Configure GPIO pins : PD12 PD13 PD14 PD15 */
	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);

		/*Configure GPIO pins : PD12 PD13 PD14 PD15 */
		GPIO_InitStruct.Pin = GPIO_PIN_15;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */


/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
BaseType_t xYieldRequired;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  } else if (htim == &pulseTimer) {
	  pulseTickCount++;
	  xYieldRequired = xTaskResumeFromISR( controlTask );
	  portYIELD_FROM_ISR(xYieldRequired);
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart1)
	{
		huart->RxXferCount = huart->RxXferSize;
		xTaskResumeFromISR(bleRxTaskHandle);
	}

	if (huart == &huart2) {
		huart-> RxXferCount = huart->RxXferSize;
		HAL_UART_Receive_IT(huart, NULL, huart->RxXferSize);
		xTaskResumeFromISR(jetiRxHandle);
	}
}

void transmitMessage(uint8_t *& p_Head, uint8_t * p_Tail) {
	do {
		osMessageQueuePut(bleTxQueueHandle, p_Head, 0, 0);
	} while (++p_Head <= p_Tail);
	osThreadResume(bleTxTaskHandle);
}

void initializeTimer() {
	__TIM2_CLK_ENABLE();

	// TIM2 -> APB1 related
	// Frequency: 48 MHz
	// 50 us * x = 1s

	pulseTimer.Instance = TIM2;
	pulseTimer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	pulseTimer.Init.CounterMode = TIM_COUNTERMODE_UP;
  
	pulseTimer.Init.Prescaler = PULSE_TIMER_PRESCALER;
	pulseTimer.Init.Period = PULSE_TIMER_INTERRUPT_COUNTS;

	pulseTimer.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&pulseTimer);

	HAL_NVIC_SetPriority(TIM2_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(TIM2_IRQn);
}

void startPulseTimer() {
	HAL_TIM_Base_Start_IT(&pulseTimer);
}

void stopPulseTimer() {
	HAL_TIM_Base_Stop_IT(&pulseTimer);
}

void applyPulse(bool isPulseHigh)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, isPulseHigh ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

#endif /* TEST */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
