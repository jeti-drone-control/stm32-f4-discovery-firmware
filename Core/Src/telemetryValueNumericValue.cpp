/*
 * telemetryValueNumeric.cpp
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#
#include <telemetryValueNumericValue.h>
#include "protocol_config.h"

using namespace Telemetry;

NumericValue::NumericValue(float value): value(value) {}

uint8_t NumericValue::getTelemetryType() {
	return TELEMETRY_PROTOCOL_TYPE_FLOAT;
}

void NumericValue::update(jeti_decoder::ExData valueUpdate) {
	value = valueUpdate.numeric;
}
