/*
 * controlDecoder.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "controlDecoder.h"
#include "protocol_config.h"
#include <cstring>

using namespace Control;

Decoder::Decoder(DecoderDelegate * p_Delegate):
		AbstractPayloadDecoder(8),
		p_Delegate(p_Delegate)
{}

uint8_t Decoder::getContentLength(uint8_t typeKey) {
	switch (typeKey) {
	case CONTROL_PROTOCOL_ACTIVATE_SIGNAL:
	case CONTROL_PROTOCOL_DEACTIVATE_SIGNAL:
		return 1;
	default:
		return 0;
	}
}

uint8_t Decoder::getProtocolId() {
	return CONTROL_PROTOCOL_ID;
}
void Decoder::payloadComplete(uint8_t payloadLength) {
	if (p_Delegate == NULL) {
		return;
	}

	switch (*p_ContentBuffer) {
	case CONTROL_PROTOCOL_ACTIVATE_SIGNAL:
		p_Delegate->activateControlSignal();
		break;
	case CONTROL_PROTOCOL_DEACTIVATE_SIGNAL:
		p_Delegate->deactivateControlSignal();
		break;
	case CONTROL_PROTOCOL_CHANNEL_VALUES:
		decodeChannelValues(payloadLength);
		break;
	default:
		return;
	}
}

void Decoder::decodeChannelValues(uint8_t payloadLength) {
	uint8_t * p_Head = p_ContentBuffer;
	uint8_t channelId;
	union {
		uint8_t bytes[4];
		float value;
	} conversion;

	while (p_Head - p_ContentBuffer < payloadLength) {
		channelId = *(++p_Head);
		memcpy(conversion.bytes, ++p_Head, 4);
		p_Head += 3;
		p_Delegate->setChannelValue(channelId, conversion.value);
	}
}


