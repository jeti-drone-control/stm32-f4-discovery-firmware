/*
 * abstractPayloadDecoder.cpp
 *
 *  Created on: 1 May 2020
 *      Author: florian.siegel
 */

#include "FreeRTOS.h"
#include "cmsis_os2.h"
#include "portable.h"
#include "abstractPayloadDecoder.hpp"

using namespace Framing;

AbstractPayloadDecoder::AbstractPayloadDecoder(uint8_t bufferLength):
				bufferLength(bufferLength) {
	p_ContentBuffer = (uint8_t*) pvPortMalloc(this->bufferLength);
	p_Write = p_ContentBuffer;
}

void AbstractPayloadDecoder::decodeValue(uint8_t value) {
	if ((uint8_t) (this->p_Write - this->p_ContentBuffer) < this->bufferLength) {
		*(this->p_Write++) = value;
	}
}

void AbstractPayloadDecoder::clear() {
	vPortFree(p_ContentBuffer);
	this->p_ContentBuffer = (uint8_t*) pvPortMalloc(this->bufferLength);
	this->p_Write = this->p_ContentBuffer;
}

void AbstractPayloadDecoder::setMessageType(uint8_t typeKey) {
	//this->messageType = typeKey;
	decodeValue(typeKey);
}

