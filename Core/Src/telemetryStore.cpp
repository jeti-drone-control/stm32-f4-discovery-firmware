/*
 * telemetryStore.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryStore.h"
#include "jeti_decoder_telemetry_ex_data.hpp"
#include "FreeRTOSConfig.h"
#include <projdefs.h>
#include <portable.h>
#include "protocol_config.h"


using namespace Telemetry;

Store::Store(
		StoreDelegate * p_Delegate,
		SystemDelegate * p_SystemDelegate):
		isTransferActive(false),
		p_Delegate(p_Delegate),
		p_SystemDelegate(p_SystemDelegate)
{}

void Store::activateTelemetryTransfer() {
	isTransferActive = true;
	confirmMessage();
}

void Store::deactivateTelemetryTransfer() {
	isTransferActive = false;
	confirmMessage();
}

void Store::scanSensors() {
	if (isTransferActive) {
		isScanning = true;
		confirmMessage();
		return;
	}

	this->neglectMessage();
}

void Store::stopSensorScan() {
	isScanning = false;
	confirmMessage();
}

void Store::subscribeSensor(
		uint16_t manufacturerId,
		uint16_t deviceId,
		uint8_t sensorId,
		uint8_t subscriptionId)
{
	uint64_t subscriptionKey = getSubscriptionKey(manufacturerId, deviceId, sensorId);
	SensorSubscription * p_Subscription = (SensorSubscription*) pvPortMalloc(sizeof(SensorSubscription));
	new (p_Subscription) SensorSubscription(subscriptionId);
	subscriptions.insert(std::pair<uint64_t, SensorSubscription*>(subscriptionKey, p_Subscription));
	p_Delegate->confirmSubscription(manufacturerId, deviceId, sensorId, subscriptionId);
}

void Store::unsubscribeSensor(uint16_t manufacturerId,
		uint16_t deviceId,
		uint8_t sensorId) {
	uint64_t subscriptionKey = getSubscriptionKey(manufacturerId, deviceId, sensorId);
	SensorSubscription* p_Subscription = subscriptions[subscriptionKey];
	if (p_Subscription == NULL) {
		// TODO sufficient response
		neglectMessage();
		return;
	}

	uint8_t subscriptionId = p_Subscription->subscriptionId;
	delete p_Subscription;
	subscriptions.erase(subscriptionKey);
	p_Delegate->confirmUnsubscription(manufacturerId, deviceId, sensorId, subscriptionId);
}

void Store::requestAlarms() {}
void Store::requestScannedSensors() {
	jeti_decoder::ExTextTelemetry *p_Telemetry;
	do {
		p_Telemetry = p_SystemDelegate->getNextIdentifiedSensor();
		if (p_Telemetry != NULL) {
			p_Delegate->sendIdentifiedSensor(p_Telemetry);
			jeti_decoder::Telemetry::destroy(p_Telemetry);
		}
	} while (p_Telemetry != NULL);
	p_Delegate->ack();
	return;
}

void Store::requestSubscriptionValues() {
	if (subscriptions.size() == 0) {
		return;
	}

	;
	for (std::map<uint64_t, SensorSubscription*>::iterator it = subscriptions.begin();
			it != subscriptions.end();
			it ++) {
		sendSubscriptionUpdate(it->second);
	}
	confirmMessage();
}

void Store::sendSubscriptionUpdate(SensorSubscription* p_Subscription) {
	TelemetryValueInterace* p_Value =  p_Subscription->getValue();
	switch(p_Value->getTelemetryType()) {
	case TELEMETRY_PROTOCOL_TYPE_DATE:
		sendDate(p_Subscription->subscriptionId, (DateValue*) p_Value);
		break;
	case TELEMETRY_PROTOCOL_TYPE_FLOAT:
		sendFloat(p_Subscription->subscriptionId, (NumericValue*) p_Value);
		break;
	case TELEMETRY_PROTOCOL_TYPE_TIME:
		sendTime(p_Subscription->subscriptionId, (TimeValue*) p_Value);
		break;
	case TELEMETRY_PROTOCOL_TYPE_GPS:
		sendGps(p_Subscription->subscriptionId, (GpsValue*) p_Value);
		break;
	}
}

void Store::sendFloat(uint8_t subscriptionId, NumericValue * p_Value)
{
	p_Delegate->encodeFloat(subscriptionId, p_Value->value);
}

void Store::sendDate(uint8_t subscriptionId, DateValue * p_Date)
{
	p_Delegate->encodeDate(subscriptionId, p_Date->day, p_Date->month, p_Date->year);
}
void Store::sendTime(uint8_t subscriptionId, TimeValue * p_Time)
{
	p_Delegate->encodeTime(subscriptionId, p_Time->hour, p_Time->minute, p_Time->second);
}
void Store::sendGps(uint8_t subscriptionId, GpsValue * p_Gps)
{
	p_Delegate->encodeGps(subscriptionId, p_Gps->value, p_Gps->orientation);
}

void Store::decodingComplete(jeti_decoder::Telemetry * p_Telemetry)
{
	if (!isTransferActive) {
		jeti_decoder::Telemetry::destroy(p_Telemetry);
		return;
	}

	switch(p_Telemetry->getType()) {
	case jeti_decoder::TelemetryType::EX_DATA:
		updateSubscription((jeti_decoder::ExDataTelemetry*) p_Telemetry);
		break;
	case jeti_decoder::TelemetryType::EX_TEXT:
		if (!isScanning && p_SystemDelegate != NULL) {
			break;
		}

		p_SystemDelegate->enqueueIdentifiedSensor((jeti_decoder::ExTextTelemetry*) p_Telemetry);
		return;
	case jeti_decoder::TelemetryType::ALARM:
		/*
		if (alarms.size() >= 3) {
			jeti_decoder::Telemetry::destroy(alarms.front());
			alarms.erase(alarms.begin());
		}
		alarms.push_back((jeti_decoder::AlarmTelemetry*) p_Telemetry);
		return;
		*/
	case jeti_decoder::TelemetryType::SIMPLE_TEXT:
	case jeti_decoder::TelemetryType::UNKOWN_TELEMETRY:
	default:
		break;
	}

	jeti_decoder::Telemetry::destroy(p_Telemetry);
	return;
}

void Store::confirmMessage() {
	if (p_Delegate != NULL) {
		p_Delegate->ack();
	}
}

void Store::neglectMessage() {
	if (p_Delegate != NULL) {
		p_Delegate->nack();
	}
}

SensorSubscription * Store::getSubscription(
		uint16_t manufacturerId,
		uint16_t deviceId,
		uint8_t measurementId) {
	const uint64_t subscriptionKey = getSubscriptionKey(manufacturerId, deviceId, measurementId);
	std::map<uint64_t, SensorSubscription*>::iterator it = subscriptions.find(subscriptionKey);
	if (it == subscriptions.end()) {
		return NULL;
	}
	return it->second;
}

void Store::updateSubscription(jeti_decoder::ExDataTelemetry * p_Telemetry) {
	SensorSubscription * p_Subscription = getSubscription(p_Telemetry->getManufacturerId(),
			p_Telemetry->getDeviceId(),
			p_Telemetry->getMeasurementId());

	if (p_Subscription == NULL) {
		return;
	}

	p_Subscription->update(p_Telemetry);
}

uint64_t Store::getSubscriptionKey(uint16_t manufacturerId, uint16_t deviceId, uint8_t measurementId)
{
	return (manufacturerId << 24) + (deviceId << 8) + measurementId;
}

void Store::reset() {
	for (std::map<uint64_t, SensorSubscription*>::iterator it = subscriptions.begin();
		it != subscriptions.end();
		it ++) {
		delete it->second;
	}
	subscriptions.clear();
	isScanning = false;
	isTransferActive = false;
}

