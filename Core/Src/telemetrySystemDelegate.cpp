/*
 * telemetrySystemDelegate.cpp
 *
 *  Created on: 16 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetrySystemDelegate.h"

extern osMessageQueueId_t identifiedSensorsQueue;

using namespace Telemetry;


SystemDelegate::SystemDelegate() {}

void SystemDelegate::enqueueIdentifiedSensor(
		jeti_decoder::ExTextTelemetry *p_Telemetry)
{
	uint32_t pointerValue = (uint32_t) p_Telemetry;
	if (osMessageQueuePut(identifiedSensorsQueue, &pointerValue, 0, 0) != osOK) {
		jeti_decoder::Telemetry::destroy(p_Telemetry);
	}
}

jeti_decoder::ExTextTelemetry * SystemDelegate::getNextIdentifiedSensor() {
	uint32_t pointerValue;
	if (osMessageQueueGetCount(identifiedSensorsQueue) == 0
			|| osMessageQueueGet(identifiedSensorsQueue, &pointerValue, 0, 0) != osOK) {
		return NULL;
	}

	return (jeti_decoder::ExTextTelemetry *) pointerValue;
}

void SystemDelegate::enqueueAlarm(
		jeti_decoder::AlarmTelemetry *p_Telemetry)
{}


