/*
 * connectivityManagementEncoder.cpp
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#include "main.h"
#include "protocol_config.h"
#include "connectivityManagementEncoder.h"
#include "frameEncoderInterface.h"

using namespace ConnectivityManagement;
Encoder::Encoder(Framing::FrameEncoderInterface* p_FrameEncoder):
	Framing::AbstractPayloadEncoder(p_FrameEncoder) {}

uint8_t ConnectivityManagement::Encoder::getProtocolId() {
	return CONNECTIVITY_PROTOCOL_ID;
}

void Encoder::encodeConnectivityStatusResponse() {
	uint8_t responseBody = CONNECTIVITY_PROTOCOL_ONLINE;
	uint8_t * p_Buffer = &responseBody;
	if (p_FrameEncoder != NULL) {
		p_FrameEncoder->encodeFraming(p_Buffer, p_Buffer, getProtocolId(), 1);
	}
}

void Encoder::encodeReset() {
	if (p_FrameEncoder == NULL) {
		return;
	}
	uint8_t responseBody = CONNECTIVITY_PROTOCOL_RESET;
	uint8_t * p_Buffer = &responseBody;
	p_FrameEncoder->encodeFraming(p_Buffer, p_Buffer, getProtocolId(), 1);

}



