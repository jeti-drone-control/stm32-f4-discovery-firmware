/*
 * rfc1662Decoder.cpp
 *
 *  Created on: 2 May 2020
 *      Author: florian.siegel
 */

#include "rfc1662Decoder.hpp"
#include "protocol-utils-constants.h"
#include "protocol-utils-escaping.h"
#include "connectivityManagementDecoder.h"
//#include "crc8.h"
#include "jeti_decoder_crc.hpp"

using namespace rfc1662;

Rfc1662Decoder::Rfc1662Decoder(std::map<uint8_t, Framing::PayloadDecoderInterface *>* p_PayloadDecoders):
		crc(0),
		frameState(NO_FRAME),
		p_CurrentPayloadDecoder(nullptr),
		subFrameLengthCounter(0),
		subFrameLength(0),
		p_PayloadDecoders(p_PayloadDecoders) {}

void Rfc1662Decoder::clear() {
	subFrameLengthCounter = 0;
	subFrameLength = 0;
	crc = 0;
	if (p_CurrentPayloadDecoder != nullptr) {
		p_CurrentPayloadDecoder->clear();
	}
	p_CurrentPayloadDecoder = nullptr;
}

void Rfc1662Decoder::decode(uint8_t value) {

 	if (frameState != PAYLOAD_COMPLETE
 			&& frameState != NO_FRAME
			&& value == RFC_1662_FRAME_FLAG) {
		frameState = NO_FRAME;
		clear();
	}

	std::map<uint8_t,Framing::PayloadDecoderInterface *>::iterator it;
	switch(frameState) {
	case NO_FRAME:
		if (value == RFC_1662_FRAME_FLAG) {
			frameState = FRAME_STARTED;
			p_CurrentPayloadDecoder = NULL;
		}
		return;
	case FRAME_STARTED:
		it = p_PayloadDecoders->find(value);
		if (it != p_PayloadDecoders->end()) {
			frameState = PROTOCOL_IDENTIFIED;
			p_CurrentPayloadDecoder = it->second;
		} else {
			frameState = NO_FRAME;
		}
		break; // 126, <PROTOCOL>, <LENGTH>, <PAYLOAD>,
	case PROTOCOL_IDENTIFIED:
		subFrameLength = value;
		crc = calculateCrc(value, crc);
		frameState = LENGTH_IDENTIFIED;
		break;
	case LENGTH_IDENTIFIED:
		p_CurrentPayloadDecoder->setMessageType(value);
		crc = calculateCrc(value, crc);
		subFrameLengthCounter++;
		frameState = FRAME_CONTENT;
		break;
	case FRAME_CONTENT:
		if (protocol_utils::isEscaped(value)) {
			frameState = ESCAPED_CONTENT;
		} else {
			p_CurrentPayloadDecoder->decodeValue(value);
		}
		crc = calculateCrc(value, crc);
		subFrameLengthCounter++;
		break;
	case ESCAPED_CONTENT:
		if (value != RFC_1662_CONTROL_ESCAPED && value != RFC_1662_FRAME_FLAG_ESCAPED) {
			frameState = rfc1662::NO_FRAME;
		} else {
			p_CurrentPayloadDecoder->decodeValue(protocol_utils::exitEscape(value));
			crc = calculateCrc(value, crc);
			subFrameLengthCounter++;
			frameState = rfc1662::FRAME_CONTENT;
		}
		break;
	case PAYLOAD_COMPLETE:
		if (this->validateCrc(value))
		{
			frameState = CRC_VALID;
		} else {
			frameState = NO_FRAME;
		}
		break;
	default:
		return;
	}


	if (frameState == CRC_VALID) {
		p_CurrentPayloadDecoder->payloadComplete(subFrameLength);
		frameState = NO_FRAME;
	}

	if (frameState > LENGTH_IDENTIFIED && subFrameLengthCounter >= subFrameLength) {
		frameState = PAYLOAD_COMPLETE;
		return;
	}

	if (frameState == NO_FRAME) {
		clear();
	}

};

bool Rfc1662Decoder::validateCrc(uint8_t controlValue) {
	return crc == controlValue;
}

// TODO remove
void Rfc1662Decoder::registerPayloadDecoder(uint8_t payloadKey, Framing::PayloadDecoderInterface * p_PayloadDecoder) {
	//payloadDecoders[payloadKey] = 1; //p_PayloadDecoder;
	return;
};
