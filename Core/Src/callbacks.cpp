/*
 * callbacks.cpp
 *
 *  Created on: 16 Aug 2020
 *      Author: florian.siegel
 */

#include "callbacks.h"
#include "cmsis_os2.h"

extern osMessageQueueId_t identifiedSensorsQueue;

void enqueueIdentifiedSensor(
		jeti_decoder::ExTextTelemetry *p_Telemetry)
{
	if (osMessageQueuePut(identifiedSensorsQueue, p_Telemetry, 0, 0) != osOK) {
		jeti_decoder::Telemetry::destroy(p_Telemetry);
	}
}

void enqueueAlarm(
		jeti_decoder::AlarmTelemetry *p_Telemetry) {
	jeti_decoder::Telemetry::destroy(p_Telemetry);
}

