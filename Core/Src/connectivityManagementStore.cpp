/*
 * connectivityManagementStore.cpp
 *
 *  Created on: 4 Aug 2020
 *      Author: florian.siegel
 */
#include "main.h"
#include "connectivityManagementStore.h"
#include "telemetryStore.h"

using namespace ConnectivityManagement;

ConnectivityManagementStore::ConnectivityManagementStore(
		StoreDelegate * p_Delegate,
		Telemetry::Store * p_TelemetryStore):
	p_Delegate(p_Delegate),
	isResetRequired(true),
	p_TelemetryStore(p_TelemetryStore)
{}

void ConnectivityManagementStore::connectivityStatusRequest() {
	if (p_Delegate == NULL) {
		return;
	}

	if (isResetRequired) {
		p_Delegate->encodeReset();
		isResetRequired = false;
	}
	p_Delegate->encodeConnectivityStatusResponse();
}

void ConnectivityManagementStore::resetRequired()
{
	p_TelemetryStore->reset();
}


