/*
 * telemetryEncoder.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryEncoder.h"
#include "protocol_config.h"
#include "cstring"

using namespace Telemetry;

Encoder::Encoder(Framing::FrameEncoderInterface* p_FrameEncoder):
		AbstractPayloadEncoder(p_FrameEncoder) {}

uint8_t Encoder::getProtocolId() {
	return TELEMETRY_PROTOCOL_ID;
}

void Encoder::ack() {
	uint8_t ackValue = TELEMETRY_PROTOCOL_ACK;
	uint8_t * p_Value = &ackValue;
	p_FrameEncoder->encodeFraming(p_Value, p_Value, getProtocolId(), 1);
}

void Encoder::nack() {
	uint8_t nackValue = TELEMETRY_PROTOCOL_NACK;
	uint8_t * p_Value = &nackValue;
	p_FrameEncoder->encodeFraming(p_Value, p_Value, getProtocolId(), 1);
}

void Encoder::sendIdentifiedSensor(jeti_decoder::ExTextTelemetry *p_Telemetry) {
	uint8_t buffer[20];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = TELEMETRY_PROTOCOL_SCAN_SENSOR_DESCRIPTION;
	*(p_Tail++) = p_Telemetry->getManufacturerId() >> 8;
	*(p_Tail++) = p_Telemetry->getManufacturerId() & 0xFF;
	*(p_Tail++) = p_Telemetry->getDeviceId() >> 8;
	*(p_Tail++) = p_Telemetry->getDeviceId() & 0xFF;
	*(p_Tail++) = p_Telemetry->getMeasurementId();
	memcpy(p_Tail, p_Telemetry->getText(), p_Telemetry->getTextLength());
	p_Tail += p_Telemetry->getTextLength() - 1;

	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 6 + p_Telemetry->getTextLength());
}

void Encoder::confirmSubscription(
		uint16_t manufacturerId,
		uint16_t deviceId,
		uint8_t sensorId,
		uint8_t subscriptionId)
{
	encodeSubscriptionConfirmation(
			TELEMETRY_PROTOCOL_SUBSCRIBE_CONFIRM,
			manufacturerId, deviceId, sensorId,
			subscriptionId);
}

void Encoder::confirmUnsubscription(
					uint16_t manufacturerId,
					uint16_t deviceId,
					uint8_t sensorId,
					uint8_t subscriptionId)
{
	encodeSubscriptionConfirmation(
				TELEMETRY_PROTOCOL_UNSUBSCRIBE_CONFIRM,
				manufacturerId, deviceId, sensorId,
				subscriptionId);
}

void Encoder::encodeSubscriptionConfirmation(
			uint8_t messageKey,
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId)
{
	uint8_t buffer[7];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = messageKey;
	*(p_Tail++) = manufacturerId >> 8;
	*(p_Tail++) = manufacturerId & 0xFF;
	*(p_Tail++) = deviceId >> 8;
	*(p_Tail++) = deviceId & 0xFF;
	*(p_Tail++) = sensorId;
	*(p_Tail) = subscriptionId;

	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 7);
}

void Encoder::encodeFloat(uint8_t subscriptionId, float value)
{
	uint8_t buffer[7];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = TELEMETRY_PROTOCOL_TYPE_FLOAT;
	*(p_Tail++) = subscriptionId;
	union {
		float value;
		unsigned char bytes[4];
	} conversion;
	conversion.value = value;
	memcpy(p_Tail, conversion.bytes, 4);
	p_Tail += 3;

	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 6);
}

void Encoder::encodeGps(uint8_t subscriptionId, float position, uint8_t orientation)
{
	uint8_t buffer[8];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = TELEMETRY_PROTOCOL_TYPE_GPS;
	*(p_Tail++) = subscriptionId;
	union {
		float value;
		unsigned char bytes[4];
	} conversion;
	conversion.value = position;
	memcpy(p_Tail, conversion.bytes, 4);
	p_Tail += 3;
	*(++p_Tail) = orientation;
	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 7);
}

void Encoder::encodeDate(uint8_t subscriptionId, uint8_t day, uint8_t month, uint16_t year)
{
	uint8_t buffer[7];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = TELEMETRY_PROTOCOL_TYPE_DATE;
	*(p_Tail++) = subscriptionId;
	*(p_Tail++) = day;
	*(p_Tail++) = month;
	*(p_Tail++) = year >> 8;
	*(p_Tail) = year & 0xFF;
	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 6);
}
void Encoder::encodeTime(uint8_t subscriptionId, uint8_t hour, uint8_t minute, uint8_t second)
{
	uint8_t buffer[6];
	uint8_t *p_Head = buffer;
	uint8_t *p_Tail = buffer;
	*(p_Tail++) = TELEMETRY_PROTOCOL_TYPE_TIME;
	*(p_Tail++) = subscriptionId;
	*(p_Tail++) = hour;
	*(p_Tail++) = minute;
	*(p_Tail) = second;
	p_FrameEncoder->encodeFraming(p_Head, p_Tail, getProtocolId(), 5);
}

