/*
 * protocl-utils-escpaing.c
 *
 *  Created on: 15 Mar 2020
 *      Author: florian.siegel
 */

#include <stdint.h>
#import "protocol-utils-escaping.h"
#import "protocol-utils-constants.h"

bool protocol_utils::isFrameHead(uint8_t value) {
	return value == RFC_1662_FRAME_FLAG;
}

bool protocol_utils::shouldEscape(uint8_t value) {
	return (value == RFC_1662_FRAME_FLAG || value == RFC_1662_CONTROL_ESCAPE);
};

bool protocol_utils::isEscaped(uint8_t value) {
	return value == RFC_1662_CONTROL_ESCAPE;
}

void protocol_utils::writeEscaped(uint8_t *& p_Buffer, uint8_t value) {
	*(p_Buffer++) = RFC_1662_CONTROL_ESCAPE;
	*(p_Buffer) = value & ~RFC_1662_ESCAPE_CHARACTER;
}

uint8_t protocol_utils::exitEscape(uint8_t *& p_Buffer) {
	if (isEscaped(*p_Buffer)) {
		p_Buffer++;
		return exitEscape(*(p_Buffer++));
	}
	return *(p_Buffer++);
}

uint8_t protocol_utils::exitEscape(uint8_t value) {
	return value | RFC_1662_ESCAPE_CHARACTER;
}
