/*
 * telemetryValueGps.cpp
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#include "telemetryValueGps.h"
#include "protocol_config.h"

using namespace Telemetry;

GpsValue::GpsValue(float position, jeti_decoder::GpsOrientation orientation):
		NumericValue(position),
		orientation(orientation) {}

uint8_t GpsValue::getTelemetryType() {
	return TELEMETRY_PROTOCOL_TYPE_GPS;
}


void GpsValue::update(jeti_decoder::ExData valueUpdate) {
	value = valueUpdate.gps.position;
}
