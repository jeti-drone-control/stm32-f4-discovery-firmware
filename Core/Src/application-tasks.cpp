/*
 * tasks.cpp
 *
 *  Created on: 3 Aug 2020
 *      Author: florian.siegel
 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */


#include "main.h"
#include "cmsis_os2.h"
#include "rfc1662Decoder.hpp"
#include "jeti_decoder_decoder_class.hpp"
#include "controlStore.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern osThreadId_t defaultTaskHandle;
extern osThreadId_t bleRxTaskHandle;
extern osThreadId_t bleTxTaskHandle;
extern osThreadId_t protocolDecoderHandle;
extern osThreadId_t connectivityTasHandle;
extern osThreadId_t jetiRxHandle;
extern osThreadId_t controlTask;

extern osMessageQueueId_t bleRxQueueHandle;
extern osMessageQueueId_t bleTxQueueHandle;
extern osMessageQueueId_t bleConnectivityRxQueueHandle;
extern osMessageQueueId_t jetiRxQueueHandle;

extern rfc1662::Rfc1662Decoder frameDecoder;
extern jeti_decoder::Decoder jetiDecoder;
extern Control::Store controlStore;

extern uint16_t pulseTickCount;

/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_startBleRxTask */
/**
* @brief Function implementing the bleRxTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_startBleRxTask */
void startBleRxTask(void *argument)
{
	//decoder.registerPayloadDecoder(1, &connectiviyManagementDecoder);

  /* USER CODE BEGIN startBleRxTask */
  /* Infinite loop */
	uint8_t bleRxValue;
	if (HAL_UART_Receive_IT(&huart1, NULL, sizeof(uint8_t)) != HAL_OK)
	{
		HAL_GPIO_WritePin(LED_PORT, LED_BLUE, GPIO_PIN_RESET);
		osThreadTerminate(bleRxTaskHandle);
		return;
	}

	for(;;)
	{
	  HAL_GPIO_TogglePin(LED_PORT, LED_BLUE);
	  while(osMessageQueueGetCount(bleRxQueueHandle) > 0) {
	  	if (osMessageQueueGet(bleRxQueueHandle, &bleRxValue, NULL, 0) == osOK)
	  	{
	  		frameDecoder.decode(bleRxValue);
	  		//osMessageQueuePut(bleTxQueueHandle, &bleRxValue, 0, 0);
	  	}
	  }
	  osThreadResume(bleTxTaskHandle);
	  //osThreadSuspend(bleRxTaskHandle);

	}
  /* USER CODE END startBleRxTask */
}

void startBleTxTask(void *argument) {
	uint8_t bleTxValue;
	HAL_UART_StateTypeDef uartState;
	HAL_GPIO_TogglePin(LED_PORT, LED_ORANGE);
	for(;;) {
		while (osMessageQueueGetCount(bleTxQueueHandle) > 0) {

			if (osMessageQueueGet(bleTxQueueHandle, &bleTxValue, NULL, 0) == osOK) {
				do {
					uartState = HAL_UART_GetState(&huart1);
				} while (uartState == HAL_UART_STATE_BUSY_TX || uartState == HAL_UART_STATE_BUSY_TX_RX);
				HAL_UART_Transmit_IT(&huart1, &bleTxValue, sizeof(uint8_t));
			}
		}
		osThreadSuspend(bleTxTaskHandle);
	}

}

/* USER CODE BEGIN Header_startProtocolDecoderTask */
/**
* @brief Function implementing the protocolDecoder thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_startProtocolDecoderTask */
void startProtocolDecoderTask(void *argument)
{
  /* USER CODE BEGIN startProtocolDecoderTask */
  /* Infinite loop */
  for(;;)
  {
    HAL_GPIO_TogglePin(LED_PORT, LED_RED);
    osDelay(1000);
  }
  return;
  /* USER CODE END startProtocolDecoderTask */
}

/* USER CODE BEGIN Header_startConnectivityTask */
/**
* @brief Function implementing the connectivityTas thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_startConnectivityTask */
void startConnectivityTask(void *argument)
{
  /* USER CODE BEGIN startConnectivityTask */
  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(LED_PORT, LED_GREEN);
	osDelay(800);
  }
  return;
  /* USER CODE END startConnectivityTask */
}

void startJetiRxTask(void *argument)
{
	uint16_t jetiRxValue;
	if (HAL_UART_Receive_IT(&huart2, NULL, sizeof(uint16_t)) != HAL_OK)
	{
		HAL_GPIO_WritePin(LED_PORT, LED_BLUE, GPIO_PIN_RESET);
		osThreadTerminate(bleRxTaskHandle);
		return;
	}

//	uint16_t values[128];
//	uint16_t *p_Head = values;
//	uint16_t *p_Tail = values + 127 * sizeof(uint16_t);
	for(;;)
	{
	  HAL_GPIO_TogglePin(LED_PORT, LED_GREEN);
	  while(osMessageQueueGetCount(jetiRxQueueHandle) > 0) {
		if (osMessageQueueGet(jetiRxQueueHandle, &jetiRxValue, NULL, 0) == osOK) // && p_Head < p_Tail)
		{
//			*(p_Head++) = jetiRxValue;
			jetiDecoder.decodeValue(jetiRxValue);
//		} else {
////			continue;
		}
	  }
//	  osDelay(500);
	  osThreadSuspend(jetiRxHandle);

	}
}

void startControlTask(void *argument)
{
	uint16_t pulseTicks = controlStore.pulseDurationExceeded() / PULSE_TIMER_FIRE_TICK_FACTOR;

	for(;;) {
		 if (pulseTicks <= pulseTickCount) {
		 	pulseTickCount = 0;
		 	pulseTicks = controlStore.pulseDurationExceeded() / PULSE_TIMER_FIRE_TICK_FACTOR;
		 }

		osThreadSuspend(controlTask);
	}
}


