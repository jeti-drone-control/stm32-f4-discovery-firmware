/*
 * controlEncoder.cpp
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#include "controlEncoder.h"
#include "protocol_config.h"

using namespace Control;

Encoder::Encoder(Framing::FrameEncoderInterface* p_FrameEncoder):
		AbstractPayloadEncoder(p_FrameEncoder)
{}

uint8_t Encoder::getProtocolId() {
	return CONTROL_PROTOCOL_ID;
}

void Encoder::ack() {
	uint8_t ackValue = CONTROL_PROTOCOL_ACK;
	uint8_t * p_Value = &ackValue;
	p_FrameEncoder->encodeFraming(p_Value, p_Value, getProtocolId(), 1);

}

void Encoder::nack() {
	uint8_t nackValue = CONTROL_PROTOCOL_ACK;
	uint8_t * p_Value = &nackValue;
	p_FrameEncoder->encodeFraming(p_Value, p_Value, getProtocolId(), 1);
}
