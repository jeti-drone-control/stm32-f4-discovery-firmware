/*
 * abstractPayloadDecoder.hpp
 *
 *  Created on: 1 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_ABSTRACTPAYLOADDECODER_HPP_
#define INC_ABSTRACTPAYLOADDECODER_HPP_

#include <stdint.h>

#include "payloadDecoderInterface.hpp"

namespace Framing {

class AbstractPayloadDecoder: public PayloadDecoderInterface {
public:
	AbstractPayloadDecoder(uint8_t bufferLength = 0);
	void decodeValue(uint8_t value) override;
	void clear() override;
	void setMessageType(uint8_t typeKey) override;
protected:
	uint8_t bufferLength;
	uint8_t * p_ContentBuffer;
	uint8_t * p_Write;
};

}



#endif /* INC_ABSTRACTPAYLOADDECODER_HPP_ */
