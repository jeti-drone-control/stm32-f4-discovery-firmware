/*
 * payloadDecoderInterface.hpp
 *
 *  Created on: 1 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_PAYLOADDECODERINTERFACE_HPP_
#define INC_PAYLOADDECODERINTERFACE_HPP_

#include "main.h"

namespace Framing {

class PayloadDecoderInterface
{
public:
	virtual ~PayloadDecoderInterface() {};
	virtual void clear() = 0;
	virtual void decodeValue(uint8_t value) = 0;
	virtual uint8_t getContentLength(uint8_t typeKey) = 0;
	virtual uint8_t getProtocolId() = 0;
	virtual void payloadComplete(uint8_t length) = 0;
	virtual void setMessageType(uint8_t typeKey) = 0;
};

}



#endif /* INC_PAYLOADDECODERINTERFACE_HPP_ */
