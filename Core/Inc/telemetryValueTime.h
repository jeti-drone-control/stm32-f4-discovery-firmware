/*
 * telemetryValueTime.h
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYVALUETIME_H_
#define INC_TELEMETRYVALUETIME_H_

#include "main.h"
#include "telemetryValueInterface.h"

namespace Telemetry {

class TimeValue: public TelemetryValueInterace
{
public:
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	TimeValue(uint8_t hour, uint8_t minute, uint8_t second);
	uint8_t getTelemetryType() override;
	void update(jeti_decoder::ExData valueUpdate) override;
private:
};

}


#endif /* INC_TELEMETRYVALUETIME_H_ */
