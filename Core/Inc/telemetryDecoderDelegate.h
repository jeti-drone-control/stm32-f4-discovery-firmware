/*
 * telemetryDecoderDelegate.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYDECODERDELEGATE_H_
#define INC_TELEMETRYDECODERDELEGATE_H_

#include "main.h"

namespace Telemetry {

class DecoderDelegate {
public:
	virtual void activateTelemetryTransfer() = 0;
	virtual void deactivateTelemetryTransfer() = 0;

	virtual void scanSensors() = 0;
	virtual void stopSensorScan() = 0;

	virtual void subscribeSensor(uint16_t manufactureId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId) = 0;
	virtual void unsubscribeSensor(
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId) = 0;

	virtual void requestAlarms() = 0;
	virtual void requestScannedSensors() = 0;
	virtual void requestSubscriptionValues() = 0;
};

}



#endif /* INC_TELEMETRYDECODERDELEGATE_H_ */
