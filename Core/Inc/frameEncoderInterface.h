/*
 * FrameEncoderInterface.hpp
 *
 *  Created on: 3 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_FRAMEENCODERINTERFACE_HPP_
#define INC_FRAMEENCODERINTERFACE_HPP_

#include <stdint.h>

namespace Framing {

class FrameEncoderInterface {
public:
	virtual void encodeFraming(uint8_t *& p_Buffer, uint8_t* p_Tail, uint8_t protocolKey, uint8_t payloadLength) = 0;
};

}


#endif /* INC_FRAMEENCODERINTERFACE_HPP_ */
