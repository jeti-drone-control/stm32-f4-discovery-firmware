/*
 * telemetryEncoder.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYENCODER_H_
#define INC_TELEMETRYENCODER_H_

#include "telemetryStoreDelegate.h"
#include "abstractPayloadEncoder.h"

namespace Telemetry {

class Encoder:
		public Framing::AbstractPayloadEncoder,
		public StoreDelegate {
public:
	Encoder(Framing::FrameEncoderInterface* p_FrameEncoder);
	virtual uint8_t getProtocolId() override;
	virtual void ack() override;
	virtual void nack() override;
	void sendIdentifiedSensor(jeti_decoder::ExTextTelemetry *p_Telemetry) override;
	void confirmSubscription(
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId) override;
	void confirmUnsubscription(
					uint16_t manufacturerId,
					uint16_t deviceId,
					uint8_t sensorId,
					uint8_t subscriptionId) override;
	void encodeFloat(uint8_t subscriptionId, float value);
	void encodeGps(uint8_t subscriptionId, float position, uint8_t orientation) override;
	void encodeDate(uint8_t subscriptionId, uint8_t day, uint8_t month, uint16_t year) override;
	void encodeTime(uint8_t subscriptionId, uint8_t hour, uint8_t minute, uint8_t second) override;

private:
	void encodeSubscriptionConfirmation(
			uint8_t messageKey,
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId);
};

}



#endif /* INC_TELEMETRYENCODER_H_ */
