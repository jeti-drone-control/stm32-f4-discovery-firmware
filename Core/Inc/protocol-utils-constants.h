/*
 * util-constants.h
 *
 *  Created on: 15 Mar 2020
 *      Author: florian.siegel
 */

#ifndef INC_UTIL_CONSTANTS_H_
#define INC_UTIL_CONSTANTS_H_

#define RFC_1662_FRAME_FLAG 		0x7E // 126
#define RFC_1662_FRAME_FLAG_ESCAPED 0x5E //
#define RFC_1662_CONTROL_ESCAPE		0x7D
#define RFC_1662_CONTROL_ESCAPED	0X5D
#define RFC_1662_ESCAPE_CHARACTER	0x20 // 32





#endif /* INC_UTIL_CONSTANTS_H_ */
