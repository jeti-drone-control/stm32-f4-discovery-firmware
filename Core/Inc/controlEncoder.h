/*
 * controlEncoder.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONTROLENCODER_H_
#define INC_CONTROLENCODER_H_

#include "controlStoreDelegate.h"
#include "frameEncoderInterface.h"
#include "abstractPayloadEncoder.h"

namespace Control {

class Encoder:
		public StoreDelegate,
		public Framing::AbstractPayloadEncoder {
public:
	Encoder(Framing::FrameEncoderInterface* p_FrameEncoder);
	uint8_t getProtocolId() override;
	void ack() override;
	void nack() override;

};

}



#endif /* INC_CONTROLENCODER_H_ */
