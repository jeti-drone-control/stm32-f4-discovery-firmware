/*
 * telemetryValueDate.h
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYVALUEDATE_H_
#define INC_TELEMETRYVALUEDATE_H_

#include "main.h"
#include "telemetryValueInterface.h"

namespace Telemetry {

class DateValue: public TelemetryValueInterace
{
public:
	uint8_t day;
	uint8_t month;
	uint16_t year;
	DateValue(uint8_t day, uint8_t month, uint16_t year);
	uint8_t getTelemetryType() override;
	void update(jeti_decoder::ExData valueUpdate) override;

private:
};

}



#endif /* INC_TELEMETRYVALUEDATE_H_ */
