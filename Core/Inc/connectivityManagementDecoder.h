/*
 * pingPongDecoder.h
 *
 *  Created on: 4 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONNETIVITYMANAGEMENTDECODER_H_
#define INC_CONNETIVITYMANAGEMENTDECODER_H_

#include "abstractPayloadDecoder.hpp"
#include "connectivityManagementDecoderDelegate.h"

#define CONNECTIVITY_PING_MESSAGE_KEY 1
#define CONNECTIVITY_PONG_MESSAGE_KEY 1

namespace ConnectivityManagement {

class Decoder: public Framing::AbstractPayloadDecoder {

public:
	Decoder(DecoderDelegate * p_Delegate);
	uint8_t getProtocolId() override;
protected:
	DecoderDelegate * p_Delegate;
	uint8_t getContentLength(uint8_t typeKey) override;
	void payloadComplete(uint8_t payloadLength) override;

};

}



#endif /* INC_CONNETIVITYMANAGEMENTDECODER_H_ */
