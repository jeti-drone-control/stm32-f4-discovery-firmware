/*
 * protocol-utils-escaping.h
 *
 *  Created on: 15 Mar 2020
 *      Author: florian.siegel
 */

#ifndef INC_PROTOCOL_UTILS_ESCAPING_H_
#define INC_PROTOCOL_UTILS_ESCAPING_H_

#include <stdint.h>

namespace protocol_utils {
	bool isFrameHead(uint8_t value);
	bool shouldEscape(uint8_t value);
	bool isEscaped(uint8_t value);
	void writeEscaped(uint8_t *& p_Buffer, uint8_t value);
	uint8_t exitEscape(uint8_t *& p_Buffer);
	uint8_t exitEscape(uint8_t value);

}




#endif /* INC_PROTOCOL_UTILS_ESCAPING_H_ */
