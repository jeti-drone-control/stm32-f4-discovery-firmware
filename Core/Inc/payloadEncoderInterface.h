/*
 * payloadEncoderInterface.h
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_PAYLOADENCODERINTERFACE_H_
#define INC_PAYLOADENCODERINTERFACE_H_

#include "main.h"

namespace Framing {

class PayloadEncoderInterface {
public:
	virtual uint8_t getProtocolId() = 0;
};

}



#endif /* INC_PAYLOADENCODERINTERFACE_H_ */
