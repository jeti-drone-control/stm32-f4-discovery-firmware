/*
 * abstractFrameEncoder.hpp
 *
 *  Created on: 3 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_ABSTRACTPAYLOADENCODER_HPP_
#define INC_ABSTRACTPAYLOADENCODER_HPP_

#include "frameEncoderInterface.h"
#include "payloadMessageInterface.h"
#include "payloadEncoderInterface.h"

namespace Framing {

class AbstractPayloadEncoder: public PayloadEncoderInterface {
public:
	AbstractPayloadEncoder(FrameEncoderInterface* p_FrameEncoder);
	void encodePayload(PayloadMessageInterface * p_PayloadMessage);
	bool isReady();
protected:
	FrameEncoderInterface* p_FrameEncoder;
};

}

#endif /* INC_ABSTRACTPAYLOADENCODER_HPP_ */
