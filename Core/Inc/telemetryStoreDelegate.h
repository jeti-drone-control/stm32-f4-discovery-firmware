/*
 * telemetryStoreDelegate.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYSTOREDELEGATE_H_
#define INC_TELEMETRYSTOREDELEGATE_H_

#include "jeti_decoder_telemetry_ex_text.hpp"

namespace Telemetry {

class StoreDelegate {
public:
	virtual void ack() = 0;
	virtual void nack() = 0;
	virtual void sendIdentifiedSensor(jeti_decoder::ExTextTelemetry *p_Telemetry) = 0;
	virtual void confirmSubscription(
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId) = 0;
	virtual void confirmUnsubscription(
				uint16_t manufacturerId,
				uint16_t deviceId,
				uint8_t sensorId,
				uint8_t subscriptionId) = 0;
	virtual void encodeFloat(uint8_t subscriptionId, float value) = 0;
	virtual void encodeGps(uint8_t subscriptionId, float position, uint8_t orientation) = 0;
	virtual void encodeDate(uint8_t subscriptionId, uint8_t day, uint8_t month, uint16_t year) = 0;
	virtual void encodeTime(uint8_t subscriptionId, uint8_t hour, uint8_t minute, uint8_t second) = 0;
};

}


#endif /* INC_TELEMETRYSTOREDELEGATE_H_ */
