/*
 * payloadDecoder.hpp
 *
 *  Created on: 2 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_PAYLOADDECODER_HPP_
#define INC_PAYLOADDECODER_HPP_

#define BUFFER_LENGTH 16

#include "abstractPayloadDecoder.hpp"

class PayloadDecoder : public ::testing::Test, public payloadDecoder::AbstractPayloadDecoder {
public:
	PayloadDecoder(): AbstractPayloadDecoder(BUFFER_LENGTH) {

	};
	uint8_t getContentLength(uint8_t typeKey) override { return 0; };
	void payloadComplete() override {};

};

#endif /* INC_PAYLOADDECODER_HPP_ */
