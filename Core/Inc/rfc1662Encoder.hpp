/*
 * rfc166Encoder.hpp
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_RFC166ENCODER_HPP_
#define INC_RFC166ENCODER_HPP_

#include "frameEncoderInterface.h"

namespace Rfc1662 {

class Rfc1662Encoder: public Framing::FrameEncoderInterface {
public:
	Rfc1662Encoder(void (*p_Callback)(uint8_t *&, uint8_t*));
	virtual void encodeFraming(uint8_t *& p_Head, uint8_t* p_Tail, uint8_t protocolKey, uint8_t payloadLength) override;
protected:
	void (*p_Callback)(uint8_t*&, uint8_t*);
};

}



#endif /* INC_RFC166ENCODER_HPP_ */
