/*
 * frameState.hpp
 *
 *  Created on: 2 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_FRAMESTATE_HPP_
#define INC_FRAMESTATE_HPP_

namespace rfc1662 {

enum FrameState {
	NO_FRAME,
	FRAME_STARTED,
	PROTOCOL_IDENTIFIED,
	LENGTH_IDENTIFIED,
	FRAME_CONTENT,
	ESCAPED_CONTENT,
	PAYLOAD_COMPLETE,
	CRC_VALID
};

}


#endif /* INC_FRAMESTATE_HPP_ */
