/*
 * connectivityManagementStore.h
 *
 *  Created on: 4 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONNECTIVITYMANAGEMENTSTORE_H_
#define INC_CONNECTIVITYMANAGEMENTSTORE_H_

#include "connectivityManagementDecoderDelegate.h"
#include "connectivityManagementStoreDelegate.h"
#include "telemetryStore.h"

namespace ConnectivityManagement {
class ConnectivityManagementStore: public DecoderDelegate {
public:
	ConnectivityManagementStore(StoreDelegate * p_Delegate, Telemetry::Store * p_TelemetryStore);
	void connectivityStatusRequest() override;
	void resetRequired() override;
protected:
	Telemetry::Store * p_TelemetryStore;
	StoreDelegate * p_Delegate;
	bool isResetRequired;

};
}

#endif /* INC_CONNECTIVITYMANAGEMENTSTORE_H_ */
