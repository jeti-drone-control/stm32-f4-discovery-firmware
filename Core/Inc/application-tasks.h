/*
 * tasks.h
 *
 *  Created on: 3 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_APPLICATION_TASKS_H_
#define INC_APPLICATION_TASKS_H_

void StartDefaultTask(void *argument);
void startBleRxTask(void *argument);
void startBleTxTask(void *argument);
void startProtocolDecoderTask(void *argument);
void startConnectivityTask(void *argument);
void startJetiRxTask(void *argument);
void startControlTask(void *argument);

#endif /* INC_APPLICATION_TASKS_H_ */
