/*
 * controlStore.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONTROLSTORE_H_
#define INC_CONTROLSTORE_H_

#define PULSE_TIMER_SYSTEM_FREQUENCY_Hz 48000000.0
#define PULSE_TIMER_FIRE_TICK_FACTOR 100.0 // declaration is wrong
#define PULSE_TIMER_FIRE_FREQUENCY_Hz	20000.0
#define PULSE_TIMER_INTERRUPT_COUNTS 1.0 
#define PULSE_TIMER_PRESCALER (PULSE_TIMER_SYSTEM_FREQUENCY_Hz / (PULSE_TIMER_INTERRUPT_COUNTS * PULSE_TIMER_FIRE_FREQUENCY_Hz) - 1)

// #define PULSE_TIMER_PRESCALER PULSE_TIMER_FREQUENCY_Hz / 1000000.0 * PULSE_TIMER_FIRE_PERIOD_US - 1

// #define PULSE_TIMER_PRESCALER 48000 - 1

#define PULSE_TIMER_UPDATE_FREQUENCY_Hz (PULSE_TIMER_FREQUENCY_Hz / (PULSE_TIMER_PRESCALER + 1))
#define PULSE_TIMER_UPDATE_FREQUENCY_uHz (PULSE_TIMER_UPDATE_FREQUENCY_Hz / 1000000.0)
#define PULSE_TIMER_TICKS_PER_PERIOD_US(period) (period * PULSE_TIMER_UPDATE_FREQUENCY_uHz)

#define DEFAULT_CONTROL_PULSE_PERIOD_US 		20000
#define MINIMUM_PPM_FRAME_LENGTH_US				20000
#define MINIMUM_PPM_VALUE_LENGTH_100_US			700.0		// Equivalent -100 %
#define CENTER_PPM_VALUE_LENGTH_US				1200.0	// Equivalent 0%
#define MAXIMUM_PPM_VALUE_LENGTH_100_US			1700.0		// Equivalent +100 %
#define MINIMUM_PPM_SYNCHRONZIATION_LENGTH_US	4000
#define PPM_VALUE_SEPARATION_LENGTH_US			300.0
#define CALCULATE_PPM_PULSE_LENGTH_US(percentage) (percentage * (MAXIMUM_PPM_VALUE_LENGTH_100_US - CENTER_PPM_VALUE_LENGTH_US)+ CENTER_PPM_VALUE_LENGTH_US) 

#define MINIMUM_CHANNELS						8 
#define MAXIMUM_CHANNELS						16


// 750 - 300 = 450
// 2250 - 300 = 1950
// 1500 - 300 = 1200
// 450 + 1950 / 2 = 200
#include "controlDecoderDelegate.h"
#include "controlStoreDelegate.h"
#include <map>

namespace Control {

class Store: public DecoderDelegate {
public:
	Store(StoreDelegate * p_Delegate,
			void (*applyPulseCallback)(bool isPulseHigh),
			void (*startTimerCallback)(),
			void (*stopTimerCallback)());
	void activateControlSignal() override;
	void deactivateControlSignal() override;
	void setChannelValue(uint8_t channelId, float value) override;
	uint64_t pulseDurationExceeded();
protected:
	StoreDelegate * p_Delegate;
	bool isControlling;
	std::map<int, float> channelValues;

	/* Debug */
	uint8_t channelCount;
	uint8_t maxChannelId;
	bool isPulseHigh; 
	uint16_t totalFrameLength;
	/* Debug */

	void (*applyPulseCallback)(bool isPulseHigh);
	void (*startTimerCallback)();
	void (*stopTimerCallback)();
};
}



#endif /* INC_CONTROLSTORE_H_ */
