/*
 * ConnectivityManagementDecoderDelegate.h
 *
 *  Created on: 4 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONNECTIVITYMANAGEMENTDECODERDELEGATE_H_
#define INC_CONNECTIVITYMANAGEMENTDECODERDELEGATE_H_

namespace ConnectivityManagement {

class DecoderDelegate {
public:
	virtual void connectivityStatusRequest() = 0;
	virtual void resetRequired() = 0;
};
}



#endif /* INC_CONNECTIVITYMANAGEMENTDECODERDELEGATE_H_ */
