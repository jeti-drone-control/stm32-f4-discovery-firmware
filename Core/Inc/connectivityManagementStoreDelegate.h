/*
 * connectivityManagementStoreDelegate.h
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONNECTIVITYMANAGEMENTSTOREDELEGATE_H_
#define INC_CONNECTIVITYMANAGEMENTSTOREDELEGATE_H_

namespace ConnectivityManagement {

class StoreDelegate {
public:
	virtual void encodeReset() = 0;
	virtual void encodeConnectivityStatusResponse() = 0;
};
}





#endif /* INC_CONNECTIVITYMANAGEMENTSTOREDELEGATE_H_ */
