/*
 * telemetryValueNumeric.h
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYVALUENUMERICVALUE_H_
#define INC_TELEMETRYVALUENUMERICVALUE_H_

#include "main.h"
#include "telemetryValueInterface.h"

namespace Telemetry {

class NumericValue: public TelemetryValueInterace
{
public:
	NumericValue(float value);
	uint8_t getTelemetryType() override;
	void update(jeti_decoder::ExData valueUpdate) override;
	float value;
};

}

#endif /* INC_TELEMETRYVALUENUMERICVALUE_H_ */
