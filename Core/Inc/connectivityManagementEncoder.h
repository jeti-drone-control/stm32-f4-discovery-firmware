/*
 * connectivityManagementEncoder.h
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONNECTIVITYMANAGEMENTENCODER_H_
#define INC_CONNECTIVITYMANAGEMENTENCODER_H_

#include "main.h"
#include "connectivityManagementStoreDelegate.h"
#include "abstractPayloadEncoder.h"

namespace ConnectivityManagement {

class Encoder: public StoreDelegate, public Framing::AbstractPayloadEncoder {
public:
	Encoder(Framing::FrameEncoderInterface* p_FrameEncoder);
	void encodeConnectivityStatusResponse() override;
	void encodeReset() override;
	uint8_t getProtocolId() override;

};
}



#endif /* INC_CONNECTIVITYMANAGEMENTENCODER_H_ */
