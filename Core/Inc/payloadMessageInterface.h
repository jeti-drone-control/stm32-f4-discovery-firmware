/*
 * payloadMessageInterface.h
 *
 *  Created on: 5 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_PAYLOADMESSAGEINTERFACE_H_
#define INC_PAYLOADMESSAGEINTERFACE_H_

#include "main.h"

namespace Framing {

class PayloadMessageInterface {
public:
	virtual uint8_t * getBufferHead() = 0;
	virtual uint8_t getLength() = 0;
};

}



#endif /* INC_PAYLOADMESSAGEINTERFACE_H_ */
