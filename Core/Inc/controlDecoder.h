/*
 * controlDecoder.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONTROLDECODER_H_
#define INC_CONTROLDECODER_H_

#include "main.h"
#include "abstractPayloadDecoder.hpp"
#include "controlDecoderDelegate.h"

namespace Control {

class Decoder:
		public Framing::AbstractPayloadDecoder {
public:
	Decoder(DecoderDelegate * p_Delegate);
	uint8_t getContentLength(uint8_t typeKey) override;
	uint8_t getProtocolId() override;
	void payloadComplete(uint8_t payloadLength) override;
protected:
	DecoderDelegate * p_Delegate;
	void decodeChannelValues(uint8_t payloadLength);
};

}
#endif /* INC_CONTROLDECODER_H_ */
