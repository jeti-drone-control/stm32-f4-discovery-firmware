/*
 * Rfc1662Decoder.hpp
 *
 *  Created on: 2 May 2020
 *      Author: florian.siegel
 */

#ifndef INC_RFC1662DECODER_HPP_
#define INC_RFC1662DECODER_HPP_

#include <map>
#include "payloadDecoderInterface.hpp"
#include "frameState.hpp"

namespace rfc1662 {

class Rfc1662Decoder {
public:
	Rfc1662Decoder(std::map<uint8_t, Framing::PayloadDecoderInterface *>* p_PayloadDecoders);
	void decode(uint8_t value);
	void registerPayloadDecoder(uint8_t payloadKey, Framing::PayloadDecoderInterface * p_Decoder);
protected:
	FrameState frameState;
	std::map<uint8_t, Framing::PayloadDecoderInterface *> *p_PayloadDecoders;
	Framing::PayloadDecoderInterface * p_CurrentPayloadDecoder;
	uint8_t subFrameLengthCounter;
	uint8_t subFrameLength;
private:
	uint8_t crc;
	void clear();
	bool validateCrc(uint8_t controlValue);
};

}




#endif /* INC_RFC1662DECODER_HPP_ */
