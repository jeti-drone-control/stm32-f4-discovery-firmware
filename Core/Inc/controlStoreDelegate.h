/*
 * controlStoreDelegate.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONTROLSTOREDELEGATE_H_
#define INC_CONTROLSTOREDELEGATE_H_

namespace Control {

class StoreDelegate {
public:
	virtual void ack() = 0;
	virtual void nack() = 0;
};

}



#endif /* INC_CONTROLSTOREDELEGATE_H_ */
