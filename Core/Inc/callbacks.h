/*
 * callbacks.h
 *
 *  Created on: 16 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CALLBACKS_H_
#define INC_CALLBACKS_H_

#include "main.h"
#include "jeti_decoder_telemetry_ex_text.hpp"
#include "jeti_decoder_telemetry_alarm.hpp"

void enqueueIdentifiedSensor(
		jeti_decoder::ExTextTelemetry *p_Telemetry);

void enqueueAlarm(
		jeti_decoder::AlarmTelemetry *p_Telemetry);



#endif /* INC_CALLBACKS_H_ */
