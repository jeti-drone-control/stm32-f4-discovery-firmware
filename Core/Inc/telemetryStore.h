/*
 * telemetryStore.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYSTORE_H_
#define INC_TELEMETRYSTORE_H_

#include <vector>
#include <map>
#include "main.h"
#include "telemetryDecoderDelegate.h"
#include "telemetryStoreDelegate.h"
#include "telemetrySystemDelegate.h"
#include "jeti_decoder_decoding_complete_handler.hpp"
#include "telemetrySensorSubscription.h"
#include "jeti_decoder_telemetry_alarm.hpp"
#include "jeti_decoder_telemetry_ex_text.hpp"
#include "jeti_decoder_telemetry_ex_data.hpp"
#include "telemetryValueDate.h"
#include "telemetryValueGps.h"
#include "telemetryValueNumericValue.h"
#include "telemetryValueTime.h"

namespace Telemetry {

class Store:
		public DecoderDelegate,
		public jeti_decoder::DecodingCompleteHandler
{
public:
	Store(
			StoreDelegate * p_Delegate,
			SystemDelegate * p_SystemDelegate = NULL);
	/*
	 * Driven by master device
	 */
	void activateTelemetryTransfer() override;
	void deactivateTelemetryTransfer() override;

	void scanSensors() override;
	void stopSensorScan() override;

	void subscribeSensor(
			uint16_t manufactureId,
			uint16_t deviceId,
			uint8_t sensorId,
			uint8_t subscriptionId) override;
	void unsubscribeSensor(
			uint16_t manufacturerId,
			uint16_t deviceId,
			uint8_t sensorId) override;

	void requestAlarms() override;
	void requestScannedSensors() override;
	void requestSubscriptionValues() override;
	/*
	 * Received from transmitter device
	 */
	void decodingComplete(jeti_decoder::Telemetry * p_Telemetry) override;

	void reset();
protected:
	bool isTransferActive;
	bool isScanning;
	std::map<uint64_t, SensorSubscription*> subscriptions;

	StoreDelegate * p_Delegate;
	SystemDelegate * p_SystemDelegate;

	void sendSubscriptionUpdate(SensorSubscription* p_Subscription);
	void sendFloat(uint8_t subscriptionId, NumericValue * p_Value);
	void sendDate(uint8_t subscriptionId, DateValue * p_Date);
	void sendTime(uint8_t subscriptionId, TimeValue * p_Time);
	void sendGps(uint8_t subscriptionId, GpsValue * p_Gps);
private:
	uint64_t getSubscriptionKey(uint16_t manufacturerId, uint16_t deviceId, uint8_t measurementId);
	void confirmMessage();
	void neglectMessage();
	SensorSubscription * getSubscription(uint16_t manufacturerId, uint16_t deviceId, uint8_t measurementId);
	void updateSubscription(jeti_decoder::ExDataTelemetry * p_TelemetyUpdate);
};

}



#endif /* INC_TELEMETRYSTORE_H_ */
