/*
 * telemetryDecoder.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYDECODER_H_
#define INC_TELEMETRYDECODER_H_

#include "main.h"
#include "abstractPayloadDecoder.hpp"
#include "telemetryDecoderDelegate.h"

namespace Telemetry {

class Decoder: public Framing::AbstractPayloadDecoder {
public:
	Decoder(DecoderDelegate * p_Delegate);
	uint8_t getProtocolId() override;
protected:
	DecoderDelegate * p_Delegate;
	uint8_t getContentLength(uint8_t typeKey) override;
	void payloadComplete(uint8_t payloadLength) override;
private:
	uint16_t decodeUint16(uint8_t * p_Value);
};

}



#endif /* INC_TELEMETRYDECODER_H_ */
