/*
 * config.h
 *
 *  Created on: 3 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

void configureFreeRtosTasks();
void configureFreeRtosQueues();
void configureFreeRtosTimer();

#endif /* INC_CONFIG_H_ */
