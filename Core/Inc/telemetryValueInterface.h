/*
 * telemetryValueInterface.h
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYVALUEINTERFACE_H_
#define INC_TELEMETRYVALUEINTERFACE_H_

#include "jeti_decoder_data.hpp"

namespace Telemetry {

class TelemetryValueInterace {
public:
	virtual void update(jeti_decoder::ExData valueUpdate) = 0;
	virtual uint8_t getTelemetryType() = 0;
};

}

#endif /* INC_TELEMETRYVALUEINTERFACE_H_ */
