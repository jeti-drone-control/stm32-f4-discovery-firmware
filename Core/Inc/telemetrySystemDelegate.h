/*
 * telemetryScoreSystemDelegate.h
 *
 *  Created on: 16 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYSYSTEMDELEGATE_H_
#define INC_TELEMETRYSYSTEMDELEGATE_H_

#include "main.h"
#include "cmsis_os2.h"

#include "jeti_decoder_telemetry_ex_text.hpp"
#include "jeti_decoder_telemetry_alarm.hpp"

//extern osMessageQueueId_t identifiedSensorsQueue;

namespace Telemetry {

class SystemDelegate {
public:
	SystemDelegate();
	osMessageQueueId_t identifiedSensorsQueue;
	void enqueueIdentifiedSensor(
			jeti_decoder::ExTextTelemetry *p_Telemetry);
	jeti_decoder::ExTextTelemetry * getNextIdentifiedSensor();
	void enqueueAlarm(
			jeti_decoder::AlarmTelemetry *p_Telemetry);
protected:


};
}



#endif /* INC_TELEMETRYSYSTEMDELEGATE_H_ */
