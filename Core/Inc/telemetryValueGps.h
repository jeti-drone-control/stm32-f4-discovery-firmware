/*
 * telemetryValueGps.h
 *
 *  Created on: 9 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_TELEMETRYVALUEGPS_H_
#define INC_TELEMETRYVALUEGPS_H_

#include <telemetryValueNumericValue.h>
#include "jeti_decoder_telemetry_ex_data.hpp"

namespace Telemetry {

class GpsValue: public NumericValue
{
public:
	const jeti_decoder::GpsOrientation orientation;
	GpsValue(float position, jeti_decoder::GpsOrientation orientation);
	uint8_t getTelemetryType() override;
	void update(jeti_decoder::ExData valueUpdate) override;

private:

};

}



#endif /* INC_TELEMETRYVALUEGPS_H_ */
