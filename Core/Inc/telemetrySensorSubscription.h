/*
 * sensorSubscription.h
 *
 *  Created on: 7 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_SENSORSUBSCRIPTION_H_
#define INC_SENSORSUBSCRIPTION_H_

#include "main.h"
#include "telemetryValueInterface.h"
#include "jeti_decoder_telemetry_ex_data.hpp"

namespace Telemetry {

class SensorSubscription {
public:
	SensorSubscription(uint8_t subscriptionId);
	 const uint8_t subscriptionId;
	 void update(jeti_decoder::ExDataTelemetry * p_Telemetry);
	 TelemetryValueInterace* getValue();



protected:
	 TelemetryValueInterace * p_Value;

private:
	 bool isBlocked;

};

}

#endif /* INC_SENSORSUBSCRIPTION_H_ */
