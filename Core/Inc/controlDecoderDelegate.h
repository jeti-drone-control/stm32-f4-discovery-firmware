/*
 * controlDecoderDelegate.h
 *
 *  Created on: 6 Aug 2020
 *      Author: florian.siegel
 */

#ifndef INC_CONTROLDECODERDELEGATE_H_
#define INC_CONTROLDECODERDELEGATE_H_

#include "main.h"

namespace Control {

class DecoderDelegate {
public:
	virtual void activateControlSignal() = 0;
	virtual void deactivateControlSignal() = 0;
	virtual void setChannelValue(uint8_t channelId, float value) = 0;
};

}



#endif /* INC_CONTROLDECODERDELEGATE_H_ */
